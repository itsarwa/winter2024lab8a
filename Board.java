public class Board {
    private Tile[][] grid;

    public Board() {
        // Initializing grid 3x3
        grid = new Tile[3][3];

        // Initializing each tile inside grid as blank
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = Tile.BLANK;
            }
        }
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                result += grid[i][j].getName() + " ";
            }
            result += "\n";
        }
        return result;
    }

    public boolean placeToken(int row, int col, Tile playerToken) {
        // Checking if row and column values are in the correct range
        if (row < 0 || row >= grid.length || col < 0 || col >= grid[0].length) {
            return false; // Invalid position
        }

        // Check if the grid at the given row and column is empty (BLANK)
        if (grid[row][col] == Tile.BLANK) {
            grid[row][col] = playerToken; // Place the player's token
            return true;
        } else {
            return false; // Position is already occupied
        }
    }

    public boolean checkIfFull() {
        // Interating through grid to check if there is a blank value
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == Tile.BLANK) {
                    return false; // if found blank value, return false
                }
            }
        }
        return true; // if no blank value found
    }

    private boolean checkIfWinningHorizontal(Tile playerToken) {
        // Iterating through each row 
        for (int i = 0; i < grid.length; i++) {
            boolean isWinning = true;
            // Checking if all tiles in the current row are equal to the playerToken
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] != playerToken) {
                    isWinning = false;
                    break;
                }
            }
            // If all tiles in the current row are equal to playerToken, return true
            if (isWinning) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfWinningVertical(Tile playerToken) {
        // Iterate through each column of the grid
        for (int j = 0; j < grid[0].length; j++) {
            boolean isWinning = true;
            // Check if all tiles in the current column are equal to the playerToken
            for (int i = 0; i < grid.length; i++) {
                if (grid[i][j] != playerToken) {
                    isWinning = false;
                    break;
                }
            }
            // If all tiles in the current column are equal to playerToken, return true
            if (isWinning) {
                return true;
            }
        }
        // If no column contains all tiles equal to playerToken, return false
        return false;
    }

    public boolean checkIfWinning(Tile playerToken) {
        // Check if winning horizontally
        if (checkIfWinningHorizontal(playerToken)) {
            return true;
        }
        // Check if winning vertically
        if (checkIfWinningVertical(playerToken)) {
            return true;
        }
        // If neither horizontal nor vertical, return false
        return false;
    }
}
