import java.util.Scanner;

public class TicTacToeApp{
	public static void main(String[] args){
	Scanner scanner = new Scanner(System.in);
	//System.out.println(Tile.BLANK); // output BLANK
	//System.out.println(Tile.BLANK.getName());// output _
	
	//System.out.println(Tile.X); //Output X
	//System.out.println(Tile.X.getName());// output X
	  
	  System.out.println("Welcome to Tic-Tac-Toe!");

        // Create a Board object
        Board board = new Board();

        // Create variables
        boolean gameOver = false;
        int player = 1;
        Tile playerToken = Tile.X; // Player 1's token is always X
		Tile player2Token = Tile.O; // Player 2's token is always O
		 // Game loop
        while (!gameOver) {
            // Print the board and players tokens
			System.out.println("\n");
			System.out.println("Player 1's token: X");
			System.out.println("Player 2's token: O");
            System.out.println(board);

            // Get user input for row and column
            System.out.print("Player " + player + ", enter row (0-2): ");
            int row = scanner.nextInt();
            System.out.print("Player " + player + ", enter column (0-2): ");
            int col = scanner.nextInt();

            // Check player's turn
            if (player == 1) {
                playerToken = Tile.X;
            } else {
                playerToken = Tile.O;
            }

            // Place token on the board
            while (!board.placeToken(row, col, playerToken)) {
                System.out.println("Invalid move! Try again.");
                System.out.print("Player " + player + ", enter row (0-2): ");
                row = scanner.nextInt();
                System.out.print("Player " + player + ", enter column (0-2): ");
                col = scanner.nextInt();
            }

            // Check for a winner or tie
            if (board.checkIfWinning(playerToken)) {
				 System.out.println(board);
                System.out.println("Player " + player + " wins!");
                gameOver = true;
            } else if (board.checkIfFull()) {
                System.out.println("It's a tie!");
                gameOver = true;
            } else {
                // Switch players
                player++;
                if (player > 2) {
                    player = 1;
                }
            }
        }
	}
}